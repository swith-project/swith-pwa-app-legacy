import { observable } from 'mobx'
import UserService from '../requests/UserService'

class UserStore {
    @observable isLoggedIn = false
    @observable userData = null

    report () {

    }

    logout () {
        localStorage.removeItem('accessToken')
        this.isLoggedIn = false
        this.userData = null
    }

    getProfile () {
        return UserService.profile().then(response => response.data).then(data => {
            this.isLoggedIn = true
            this.userData = data
        }).catch(_ => {
            this.isLoggedIn = false
            this.userData = null
        })
    }
}

const userStore = new UserStore()
export default userStore