import axios from 'axios'
import UserService from './UserService'

const apiConfig = {
    baseURL: process.env.NODE_ENV === 'production' ? 'https://api.swith.online/api/' : '/api/'
    // withCredentials: true
    // method: 'GET'
}
export const requestWithToken = axios.create(apiConfig)
export const requestWithoutToken = axios.create(apiConfig)
requestWithToken.interceptors.request.use(config => {
    const token = localStorage.getItem('accessToken')
    if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
    }
    return config
})

requestWithToken.interceptors.response.use(x => x, async error => {
    const originalRequest = error.config
    if (error?.response) {
        error.message = error.response?.data?.message
        if (error.response?.status === 401 && (error.response.data.message === 'jwt expired' || error.response.data.message === 'jwt malformed') && !originalRequest._retry) {
            originalRequest._retry = true
            const access_token = await UserService.refreshToken({
                refreshToken: localStorage.getItem('refreshToken')
            }).then(response => {
                localStorage.setItem('accessToken', response.data.access_token)
                return response.data.access_token
            }).catch(error => {
                return null
            })
            axios.defaults.headers['Authorization'] = 'Bearer ' + access_token
            return requestWithToken(originalRequest)

        }
    }
    return Promise.reject(error)
})
requestWithoutToken.interceptors.response.use(x => x, async error => {


    if (error.response) {
        error.message = error.response?.data?.message

    }
    return Promise.reject(error)
})

export const fetcher = (url, term) => requestWithToken({ ...apiConfig, url, params: { term } }).then(res => res.data)