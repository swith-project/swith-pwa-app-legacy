import { requestWithoutToken, requestWithToken } from './requests'


const UserService = {
    login: code => requestWithoutToken.post(`/user/login`, code),
    logout: _ => true,
    profile: _ => requestWithToken.get(`/user/profile`),
    refreshToken: params => requestWithToken.get(`/user/refresh-token`, {
        params
    })
}

export default UserService