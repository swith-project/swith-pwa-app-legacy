import { requestWithToken } from './requests'

const RobotService = {
    search: () => requestWithToken.get(`/robot/search`),
    addDevices: board_id => requestWithToken.post(`/robot/create`, {
        board_id
    }),
    updateNameByBoardId: (board_id, body) => requestWithToken.put(`/robot/user/${board_id}`, body),
    updateSensorByBoardId: (board_id, body) => requestWithToken.put(`/robot/user/${board_id}/sensor`, body)
}

export default RobotService