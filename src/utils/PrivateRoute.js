import React, { useEffect, useState } from 'react'
import { Redirect, Route } from 'react-router-dom'
import userStore from '../store/userStore'
import LoadingScreen from '../components/common/loadingScreen'

function PrivateRoute ({ component: Component, ...rest }) {
    const [loaded, setLoaded] = useState(true)
    useEffect(() => {
        if (userStore.isLoggedIn) {
            setLoaded(false)
        }
        userStore.getProfile().then(r => {
            setLoaded(false)
        })

        return () => {
            setLoaded(true)
        }
    }, [])

    return (
        <Route
            {...rest}
            render={(props) => {
                return loaded ? (
                    <LoadingScreen/>
                ) : userStore.isLoggedIn ? <Component  {...props} /> :
                    (
                        <Redirect to='/login'/>
                    )
            }}

        />
    )
}

export default PrivateRoute