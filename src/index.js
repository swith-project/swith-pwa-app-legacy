import React from 'react'
import ReactDOM from 'react-dom'

import * as serviceWorker from './serviceWorker'
import App from './components/common/App'
import { BrowserRouter } from 'react-router-dom'
import userStore from './store/userStore'
import { Provider } from 'mobx-react'
import * as Sentry from '@sentry/react'
import { Integrations } from '@sentry/tracing'
import './index.css'

Sentry.init({
    dsn: 'https://fdbe02224cc2417dada8661e9cb63fa6@o445952.ingest.sentry.io/5423595',
    integrations: [
        new Integrations.BrowserTracing()
    ],
    tracesSampleRate: 0.5,
    release: 'my-project-name@' + process.env.npm_package_version
})
ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Provider user={userStore}>
                <App/>
            </Provider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()
