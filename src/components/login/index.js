import React, { useEffect } from 'react'
import styled from 'styled-components'
import FacebookLogin from 'react-facebook-login'
import { Card, CardBody, CardHeader } from 'reactstrap'
import { useHistory, useLocation } from 'react-router-dom'
import UserService from '../../requests/UserService'
import userStore from '../../store/userStore'
import { toast } from 'react-toastify'
import { observer } from 'mobx-react'
import { REDIRECT_URI } from '../../constants'

const LoginWrapper = styled.div`
display: flex;
height: 100vh;
justify-content: center;
align-items: start;
`

function useQuery () {
    return new URLSearchParams(useLocation().search)
}

function LoginPage () {
    const history = useHistory()
    const params = useQuery()
    const responseFacebook = (response) => {
        return UserService.login({
            access_token: response?.accessToken
        }).then(({ data }) => {
            toast.success('Login successfully!')
            localStorage.setItem('accessToken', data.access_token)
            localStorage.setItem('refreshToken', data.refresh_token)
            userStore.getProfile().then(_ => {
                if (userStore.isLoggedIn) {
                    return history.push('/')
                }
            })
        }).catch(error => {
            // toast.error(error.message)
        })
    }
    useEffect(
        () => {
            const code = params.get('code')
            if (code) {
                UserService.login({
                    code
                }).then(({ data }) => {
                    toast.success('Login successfully!')
                    localStorage.setItem('accessToken', data.access_token)
                    localStorage.setItem('refreshToken', data.refresh_token)
                    return history.push('/')
                }).catch(error => {
                    // toast.error(error.message)
                    params.delete('code')
                })
            }
        },
        [
            history,
            params
        ]
    )
    return (
        <LoginWrapper className={'d-flex justify-content-center align-items-start'}>
            <div className="col-11">
                <div
                    title="Card title"
                    // className="pt-5"
                >
                    <div className="row">
                        <div className="col-12 text-center">
                            <Card>
                                <CardHeader className={'bg-primary text-white'}>
                                    Login
                                </CardHeader>
                                <CardBody>
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries, but also the leap into electronic
                                    </p>
                                    <FacebookLogin
                                        appId="372467607082942"
                                        autoLoad={false}
                                        fields="name,email,picture"
                                        callback={responseFacebook}
                                        redirectUri={REDIRECT_URI}
                                    />
                                </CardBody>
                            </Card>
                        </div>
                    </div>
                </div>
            </div>
        </LoginWrapper>
    )
}

export default observer(LoginPage)