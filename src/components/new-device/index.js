import React, { useEffect, useState } from 'react'
import RobotService from '../../requests/RobotService'
import BackButton from '../switch/BackButton'
import { Loader, Search } from 'react-feather'
import DeviceItem from './DeviceItem'

function NewDevicePage () {
    const [devices, setDevices] = useState([])
    const [searching, setSearching] = useState(true)
    const searchDevices = async () => {
        setSearching(true)
        await RobotService.search().then(response => response.data).then(result => {
            setDevices(result)
        }).catch(error => {
            setDevices([])
        })
    }

    useEffect(() => {
        searchDevices().then(() => {
            setSearching(false)
        })

        return () => {
        }
    }, [])

    return (
        <div className={'container-fluid'}>
            <div className={'row'}>
                <div className="col-4 btn-group-sm">
                    <BackButton/>
                </div>
                <div className="col-8 d-flex align-items-center">
                    <h4>
                        Add new <b>SWiTH</b>
                    </h4>
                </div>
            </div>
            <div className="row mt-4">

                <div className="col-12">
                    <div className="card r-10">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 flex-column d-flex justify-content-center">
                                    <button onClick={_ => searchDevices().then(() => setSearching(false))}
                                            className={'btn-search-sw mx-auto mb-2 btn btn-secondary r-5'}>
                                        <Search size={30}/> <br/>
                                        Scan Now
                                    </button>
                                    <p>
                                        Tips: Please connect your device with the same internet router.
                                    </p>
                                    <hr/>

                                    {searching && (
                                        <div className={'py-5 text-center '}>
                                            <Loader size={40}
                                                    className={'spinner'}/>  <p>Searching...</p>
                                        </div>
                                    )}
                                    {devices.map((item, key) => (
                                        <DeviceItem {...item} key={key}/>
                                    ))}
                                    {!searching && devices.length > 0 && (
                                        <div className={'mb-2 text-center'}>
                                            <p>
                                                {devices.length} devices found.

                                            </p>
                                        </div>
                                    )}
                                    {!searching && devices.length === 0 && (
                                        <div className={'my-5 text-center'}>
                                            <p>Sorry, we can't find device around you.</p>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewDevicePage