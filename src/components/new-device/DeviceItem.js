import React, { useState } from 'react'
import { Check, Loader, Power, Sliders, X } from 'react-feather'
import styled from 'styled-components'
import dayjs from 'dayjs'
import RobotService from '../../requests/RobotService'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'

const DeviceItemWrapper = styled.div`
    background: #F1F1F1;
    padding: 10px 15px;
    border-radius: 5px;
    position: relative;
    .row{
    filter:blur(${props => props.selected === 0 ? '0px' : '2px'});
    }
    .confirm{
    display: ${props => props.selected === 0 ? 'none' : 'flex'};
    position: absolute;
    background: rgba(0,0,0,0.589);
    border-radius: 5px;
    width:100%;
    -webkit-animation: fadeIn .2s;
    animation: fadeIn .2s;
    top:0;
    left:0;
    height:100%;
    }
    
    @-webkit-keyframes fadeIn {
    from { opacity: 0; }
      to { opacity: 1; }
}
@keyframes fadeIn {
    from { opacity: 0; }
      to { opacity: 1; }
}
`

function DeviceItem ({ board_id, ip_address, description, created_at, ...props }) {
    const history = useHistory()
    const [isSelected, setSelected] = useState(0)
    const [loading, setLoading] = useState(false)
    const submit = async () => {
        await RobotService.addDevices(board_id).then(data => {
            history.push('/')
            toast.success('Added device successful.')
        }).catch(error => {
            toast.error('Failed to add this devices')
        })
        setLoading(false)
        setSelected(0)
    }
    return (
        <DeviceItemWrapper
            className={'d-flex mb-2'}
            selected={isSelected}
            onClick={e => {
                if (isSelected === 0) {
                    setSelected(1)
                }
            }}
        >
            {loading ? (
                <div className={'loader'}>
                    <div className="py-2 d-flex">
                        <Loader className={'spinner'}/> <p className={'ml-2'}>Adding this device into your home.</p>
                    </div>
                </div>
            ) : (
                <React.Fragment>
                    <div className="row">
                        <div className={'col-2 text-center'}>
                            <Sliders size={32}
                                     className={'mt-3'}/>
                        </div>
                        <div className={'col-10'}>
                            <h5 className={'mb-0 text-dark'}>IP: {ip_address}

                            </h5>
                            <p className={'mb-0'}>
                                SWiTH ID: <small>{board_id}</small>
                            </p>
                            <div className="float-right">
                                <small className={'text-muted text-right'}><Power size={12}/> {dayjs(created_at).format('DD/MM/YYYY hh:mm')}
                                </small>
                            </div>
                        </div>
                    </div>
                    <div className="confirm">
                        <div
                            className={'px-3 d-flex justify-content-end align-items-center w-100'}>
                            <p className={'text-white px-2'}>Are you sure to want to add this device ?</p>
                            <div className={'btn-group-sm mr-0 ml-auto d-flex flex-column'}>
                                <button
                                    onClick={e => {
                                        e.preventDefault()
                                        setLoading(true)
                                        submit()
                                    }}
                                    className={'btn btn-success'}>
                                    <Check/>
                                </button>
                                <button className={'btn btn-danger btn-sm'}
                                        onClick={e => {
                                            e.preventDefault()
                                            setSelected(0)
                                        }}
                                >
                                    <X/>
                                </button>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            )}
        </DeviceItemWrapper>
    )
}

export default DeviceItem