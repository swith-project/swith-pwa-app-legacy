import React from 'react'
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarBrand, UncontrolledDropdown } from 'reactstrap'
import { observer } from 'mobx-react'
import userStore from '../../store/userStore'

function TopHeader () {

    return (
        <div>
            <Navbar
                className={'py-0 d-flex d-md-none shadow-sm'}

                color="light"
                light

                expand="lg">
                <NavbarBrand href="/">{userStore.userData?.username}</NavbarBrand>
                <Nav>
                    <UncontrolledDropdown nav
                                          inNavbar>
                        <DropdownToggle nav
                                        caret>
                            <img className={'user-image'}
                                 src={userStore.userData?.avatar_image}
                                 alt=""/>
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                Option 1
                            </DropdownItem>
                            <DropdownItem>
                                Option 2
                            </DropdownItem>
                            <DropdownItem divider/>
                            <DropdownItem onClick={e => {
                                e.preventDefault()
                                userStore.logout()
                            }}>
                                Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </Navbar>
        </div>
    )
}

export default observer(TopHeader)