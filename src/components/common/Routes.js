import React from 'react'
import { Link, Route, Switch } from 'react-router-dom'
import PrivateRoute from '../../utils/PrivateRoute'
import LoginPage from '../login'
import Home from '../home'
import SettingPage from '../settings'
import NewDevicePage from '../new-device'
import FamilyPage from '../family'
import ManageSWiTHPageByBoardId from '../manage'
import NewSchedule from '../new-schedule'

function AppRoutes () {
    return (
        <Switch>
            <PrivateRoute
                exact
                path="/"
                component={Home}
            />
            <PrivateRoute
                exact
                path="/home"
                component={Home}
            />
            <PrivateRoute
                exact
                path="/family"
                component={FamilyPage}
            />
            <PrivateRoute
                exact
                path={'/manage/:board_id'}
                component={ManageSWiTHPageByBoardId}
            />
            <PrivateRoute
                exact
                path={'/manage/:board_id/new-schedule'}
                component={NewSchedule}
            />
            <PrivateRoute
                exact
                path={'/new/device'}
                component={NewDevicePage}
            />
            <PrivateRoute
                exact
                path="/settings"
                component={SettingPage}
            />
            <Route
                exact
                path={'/login'}
                component={LoginPage}
            />
            <Route render={props => (
                <div className={'container d-flex justify-content-center flex-column align-items-center '}
                     style={{ height: '80vh' }}>
                    <h2>Not found.</h2>
                    <Link to={'/'}
                          className={'btn btn-primary'}>
                        Go home
                    </Link>
                </div>
            )}/>
        </Switch>
    )
}

export default AppRoutes