import React, { useEffect, useState } from 'react'
import AppRoutes from './Routes'
import { inject, observer } from 'mobx-react'
import Menu from '../menu'
import TopHeader from '../top-header'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App ({ user }) {
    // eslint-disable-next-line no-unused-vars
    const [state, setState] = useState(null)
    useEffect(() => {

    }, [])
    return (
        <div className="app ">
            {user.isLoggedIn && <Menu/>}
            {user.isLoggedIn && <TopHeader/>}
            <div className={'content mt-4'}
                 style={{ paddingBottom: '160px' }}>
                <AppRoutes/>
            </div>
            <ToastContainer position={'top-center'}
                            hideProgressBar

            />
        </div>
    )
}

export default inject('user')(observer(App))