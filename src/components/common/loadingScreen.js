import React from 'react'

function LoadingScreen () {
    return (
        <div style={{
            position: 'fixed',
            height: '100%',
            top: 0,
            left: 0,
            width: '100%',
            background: 'white'
        }}
             className={'d-flex justify-content-center align-items-center'}>
            <h1 className={'text-muted'}>
                SWiTH
            </h1>
        </div>
    )
}

export default LoadingScreen