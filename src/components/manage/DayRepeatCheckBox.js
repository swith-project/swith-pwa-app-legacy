import React, { useState } from 'react'
import { FormGroup, Input, Label } from 'reactstrap'

function DayRepeatCheckBox({ is_repeat, onChange }) {
    const [repeat_on_sunday, setSunday] = useState(false)
    const [repeat_on_monday, setMonday] = useState(false)
    const [repeat_on_tuesday, setTuesday] = useState(false)
    const [repeat_on_wednesday, setWednesday] = useState(false)
    const [repeat_on_thursday, setThursday] = useState(false)
    const [repeat_on_friday, setFriday] = useState(false)

    return (
        <div className="form-group row d-flex flex-row flex-wrap">
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Sun
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Mon
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Tue
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Wed
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Thu
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Fri
                    </Label>
                </FormGroup>
            </div>
            <div className="col-3">
                <FormGroup check>
                    <Label check>
                        <Input
                            disabled={!is_repeat}
                            type="checkbox"
                            id="checkbox2"/>{' '}
                        Sat
                    </Label>
                </FormGroup>
            </div>
        </div>
    )
}

export default DayRepeatCheckBox