import React from 'react'
import useSWR from 'swr'
import { fetcher } from '../../requests/requests'
import { AlertTriangle } from 'react-feather'

function ScheduleList ({ board_id }) {
    const {
        data,
        error
    } = useSWR(`/robot/user/${board_id}/schedule`, fetcher)
    if (error) return <p className={'text-danger text-center py-5 d-flex justify-content-center align-items-center'}>
        <AlertTriangle size={14}
                       className={'mr-2'}/> Error to load schedule list.
    </p>
    return (
        <div>
            {!data && <React.Fragment>
                as
            </React.Fragment>}
        </div>
    )
}

export default ScheduleList