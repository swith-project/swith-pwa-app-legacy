import React, { useEffect, useState } from 'react'
import BackButton from '../switch/BackButton'
import useSWR from 'swr'
import { fetcher } from '../../requests/requests'
import io from 'socket.io-client'
import { SOCKET_IO } from '../../constants'
import SwitchControl from './SwitchControl'
import * as Icon from 'react-feather'
import SensorUpdateForm from './SensorUpdateForm'
import ScheduleList from './ScheduleList'
import { useHistory } from 'react-router-dom'
import SwitchManagePageSkeleton from '../skeleton/swith-manage-page'

function ManageSWiTHPageByBoardId ({ match: { params: { board_id } } }) {
    const token = localStorage.getItem('accessToken')
    const [power1, setPower1] = useState(null)
    const [power2, setPower2] = useState(null)
    const [power3, setPower3] = useState(null)
    const history = useHistory()
    const { data, error, mutate } = useSWR([
        `/robot/user/${board_id}`
    ], fetcher)

    useEffect(
        () => {
            const socket = io(SOCKET_IO)
            socket.on('connect', () => {
                socket.emit('authenticate', { token: localStorage.getItem('accessToken'), board_id })
                socket.on('update', result => {
                    setPower1(result?.servo_1)
                    setPower2(result?.servo_2)
                    setPower3(result?.servo_3)
                    console.log('update!')
                })
            })
            return () => {
                socket.disconnect()
            }
        },
        [
            token,
            board_id
        ]
    )

    return (
        <div className={'container-fluid'}>
            <div className={'row'}>
                <div className="col-3 btn-group-sm">
                    <BackButton/>
                </div>
                <div className="col-9 d-flex align-items-center justify-content-lg-start">
                    <h4>
                        {!error && data && data.label}
                    </h4>
                </div>
            </div>
            {
                !data &&
                <SwitchManagePageSkeleton/>}
            {
                !error && data &&
                <div className="card r-10 mt-4">
                    <div className="card-body">
                        <SwitchControl
                            board_id={board_id}
                            servo_1={power1}
                            servo_2={power2}
                            servo_3={power3}
                        />
                    </div>
                </div>
            }

            {!error && data && (
                <React.Fragment>
                    <div className="card r-10 mt-3">
                        <div className="card-body">
                            <h5 className={'d-flex align-items-center'}>
                                <Icon.Settings/> <span className={'ml-2'}>Sensor / Name -  Config</span>
                            </h5>
                            <SensorUpdateForm
                                {...data}
                                servo_1={power1}
                                servo_2={power2}
                                servo_3={power3}
                                mutate={mutate}
                            />
                        </div>
                    </div>
                    <div className="card r-10 my-3">
                        <div className="card-body">
                            <h5 className={'d-flex align-items-center'}>
                                <Icon.Clock/> <span className={'ml-2'}>
                                Schedule Timer
                            </span>
                                <a href="#"
                                   onClick={e => {
                                       e.preventDefault()
                                       history.push(`/manage/${board_id}/new-schedule`)
                                   }}
                                   className={'btn-sm btn ml-auto btn-info'}>
                                    <Icon.Plus size={14}/> New
                                </a>
                            </h5>
                            <ScheduleList board_id={data.board_id}/>
                        </div>
                    </div>
                </React.Fragment>
            )}
        </div>
    )
}

export default ManageSWiTHPageByBoardId