import React from 'react'
import { requestWithToken } from '../../requests/requests'
import Switch from 'react-switch'

const Item = ({ number = 1, handleClick, ...rest }) => {
    return (
        <div className="row switch-line">
            <div className="col-8">
                <h5 className={'mb-0'}>Switch {number}</h5>
            </div>
            <div className="col-4 text-right d-flex justify-content-end align-items-end">
                <Switch
                    onColor={'#28A745'}
                    // offColor={'#DC3545'}
                    onChange={async e => (await handleClick({ position: number, direction: Number(!rest[`servo_${number}`]) }))(e)}
                    checked={rest[`servo_${number}`] || false}
                />
            </div>
            <div className="col-12">
                <hr className={'my-0'}/>
            </div>
        </div>
    )
}

function SwitchControl ({ board_id, servo_1 = true, servo_2 = true, servo_3 = true }) {
    const handleClick = async ({ position: number, direction }) => {
        return async () => {
            await requestWithToken.get(`/robot/dev/${board_id}`, {
                params: {
                    direction,
                    number
                }
            })
        }
    }
    return (
        <div>
            <Item
                number={1}
                servo_1={servo_1}
                servo_2={servo_2}
                servo_3={servo_3}
                handleClick={handleClick}
            />

            <Item
                number={2}
                servo_1={servo_1}
                servo_2={servo_2}
                servo_3={servo_3}
                handleClick={handleClick}
            />
            <Item
                number={3}
                servo_1={servo_1}
                servo_2={servo_2}
                servo_3={servo_3}
                handleClick={handleClick}
            />
        </div>
    )
}

export default SwitchControl