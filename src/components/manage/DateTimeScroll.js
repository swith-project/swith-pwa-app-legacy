import React from 'react'
import Select from 'react-select'


const styles = {
    container: (s) => ({
        ...s,
        width: '46%'
    })
}

const hours_options = [...Array(25)].map((_, i) => ({
    label: `${i}`.length === 1 ? `0${i}` : i,
    value: i
}))
const minutes_options = [...Array(61)].map((_, i) => ({
    label: `${i}`.length === 1 ? `0${i}` : i,
    value: i
}))

function DateTimeScroll({ onChange }) {

    return (
        <div className="from-group col-11 col-md-10 mx-auto my-3">
            <div className="d-flex w-100 flex-row justify-content-between
            align-items-center
            ">
                <Select
                    styles={styles}
                    label={'Hours'}
                    options={hours_options}
                    placeholder={'Hours'}
                    isClearable={false}
                    isSearchable={false}
                />
                <p style={{
                    width: '8%',
                    fontSize: '2rem'
                }}
                   className={'text-center mb-0'}>
                    :
                </p>
                <Select
                    styles={styles}
                    label={'Minutes'}
                    isClearable={false}
                    isSearchable={false}
                    options={minutes_options}
                    placeholder={'Minutes'}
                />
            </div>

        </div>
    )
}

export default DateTimeScroll