import React, { useState } from 'react'
import * as Icon from 'react-feather'
import { Check, RefreshCw, X } from 'react-feather'
import { FormProvider, useForm } from 'react-hook-form'
import RobotService from '../../requests/RobotService'
import { toast } from 'react-toastify'

const renderPower = isOn => isOn ? <Icon.ToggleRight size={35}/> : <Icon.ToggleLeft size={35}/>

function SensorUpdateForm ({
    sensor_range,
    sensor_toggle_motor_mode,
    sensor_toggle_motor_no,
    label: label_props,
    servo_1,
    servo_2,
    servo_3,
    board_id,
    mutate
}) {
    const form = useForm({
        defaultValues: {
            sensor_range,
            sensor_toggle_motor_mode,
            sensor_toggle_motor_no
        }
    })
    const [motor, setMotor] = useState(sensor_toggle_motor_no)
    const [mode, setMode] = useState(sensor_toggle_motor_mode)
    const [label, setLabel] = useState(label_props)
    const [sensorRange, setSensorRange] = useState(sensor_range)
    const [loading, setLoading] = useState(false)
    const submit = async e => {
        e.preventDefault()
        setLoading(true)
        await RobotService.updateNameByBoardId(board_id, {
            label
        }).then(result => {

        }).catch(error => {
            console.log(error)
            toast.error('Failed to update name of SWiTH.')
        })
        await RobotService.updateSensorByBoardId(board_id, {
            sensor_range: sensorRange,
            sensor_mode: mode,
            sensor_no: motor
        }).then(result => {
            
        }).catch(error => {
            console.log(error)
            toast.error('Failed to update sensor.')
        })
        toast.success('Update successful.')
        mutate()
        setLoading(false)
    }

    return (
        <FormProvider {...form}>
            <form

                className={'mt-4'}
                onSubmit={submit}>
                <div className=" form-group">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text">
                                <b>Name</b>
                            </span>
                        </div>
                        <input
                            type="text"
                            maxLength={50}
                            name={'label'}
                            value={label}
                            disabled={loading}
                            onChange={event => {
                                setLabel(event.target.value)
                            }}
                            className={'form-control only-border-bottom text-right'}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text">
                                <b>Sensor Distance</b>
                            </span>
                        </div>
                        <input
                            type="number"
                            name={'sensor_range'}
                            max={300}
                            min={0}
                            disabled={loading}
                            onChange={event => {
                                setSensorRange(event.target.value)
                            }}
                            value={sensorRange}
                            className={'form-control only-border-bottom text-right'}
                        />
                        <div className="input-group-append">
                            <span className="input-group-text">
                                <small>cm</small>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <div className="btn-group w-100">
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMotor(1)
                            }}
                            type="button"
                            className={
                                motor === 1 ? `btn text-primary` : `btn`
                            }>
                            {renderPower(servo_1)} 1
                        </button>
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMotor(2)
                            }}
                            type="button"
                            className={
                                motor === 2 ? `btn text-primary` : `btn`
                            }>
                            {renderPower(servo_2)} 2
                        </button>
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMotor(3)
                            }}
                            type="button"
                            className={
                                motor === 3 ? `btn text-primary` : `btn `
                            }>
                            {renderPower(servo_3)} 3
                        </button>
                    </div>
                </div>
                <div className="form-group">
                    <div className="btn-group w-100">
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMode('up')
                            }}
                            type="button"
                            className={mode === 'up' ? `btn  text-primary` : `btn`}>
                            <Check/> On
                        </button>
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMode('down')
                            }}
                            type="button"
                            className={mode === 'down' ? `btn  text-primary` : `btn`}>
                            <X/> Off
                        </button>
                        <button
                            onClick={e => {
                                e.preventDefault()
                                setMode('toggle')
                            }}
                            type="button"
                            className={mode === 'toggle' ? `btn  text-primary` : `btn`}>
                            <RefreshCw/> Toggle
                        </button>
                    </div>
                </div>
                <div>
                    <button
                        disabled={loading}
                        className={'btn btn-primary float-right'}>
                        <Check size={16}/> {loading ? 'Saving...' : 'Save'}
                    </button>
                </div>
            </form>
        </FormProvider>

    )
}

export default SensorUpdateForm