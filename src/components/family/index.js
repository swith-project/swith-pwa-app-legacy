import React from 'react'
import * as Icon from 'react-feather'

function FamilyPage () {
    return (
        <div className={'container-fluid'}>
            <div className="card r-10">
                <div className="card-body">
                    <h3 className={'d-flex align-items-center'}>
                        <Icon.Users/> <span className={'ml-2'}>Family</span>
                    </h3>
                    <p className={'py-4'}>
                        You can share you SWiTH devices together on the same house.
                    </p>

                    <small className={"float-right"}>
                        Coming Soon...
                    </small>
                </div>
            </div>
        </div>
    )
}

export default FamilyPage