import React from 'react'
import { SkipBack } from 'react-feather'
import { useHistory } from 'react-router-dom'

function BackButton () {
    const history = useHistory()
    return (
        <button
            onClick={e => {
                e.preventDefault()
                history.goBack()
            }}
            style={{
                borderRadius: '10px'
            }}
            className={'  btn-sm btn btn-info d-flex align-items-center justify-content-center'}>
            <SkipBack size={14}/> Back
        </button>
    )
}

export default BackButton