import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import PowerButton from '../home/PowerButton'
import { useHistory } from 'react-router-dom'
import io from 'socket.io-client'
import { SOCKET_IO } from '../../constants'

export const SwitchWrapper = styled.div`
    border: 1px solid #ccc;
    margin-bottom: 20px;
    padding: 15px 15px;
    border-radius: 20px;
    box-shadow: rgba(0,0,0,0.16) 0 3px 6px;
    .header{
      margin-bottom: 20px;
    }
`

function SwitchItem ({
    label = 'Bathroom', board_id = '002482124600000000',
    servo_2,
    servo_1,
    servo_3,
    ip_address, status_available = 3
}) {

    const [power1, setPower1] = useState(servo_1)
    const [power2, setPower2] = useState(servo_2)
    const [power3, setPower3] = useState(servo_3)
    const history = useHistory()
    useEffect(
        () => {
            const socket = io(SOCKET_IO)
            socket.on('connect', () => {
                socket.emit('authenticate', { token: localStorage.getItem('accessToken'), board_id })
                socket.on('update', result => {
                    setPower1(result.servo_1)
                    setPower2(result.servo_2)
                    setPower3(result.servo_3)
                })
            })
            return () => {
                socket.disconnect()
            }
        },
        [
            board_id
        ]
    )

    return (
        <SwitchWrapper
            className={'bg-white '}>
            <div
                onClick={(e) => {
                    e.preventDefault()
                    return history.push('/manage/' + board_id)
                }}

                className={'header text-center'}>
                <h4>{label}</h4>
                <p>IP: {ip_address.replace('::ffff:', '')}</p>
            </div>
            <div>
                <div className="d-flex justify-content-between">
                    <PowerButton
                        isOn={power1}
                        position={1}
                        // onChange={value => {
                        //     setPower1(!power1)
                        // }}
                        boardId={board_id}
                        isUnavailable={status_available === 0}
                    />
                    <PowerButton
                        isOn={power2}
                        position={2}
                        // onChange={value => {
                        //     setPower2(!power2)
                        // }}
                        boardId={board_id}
                        isUnavailable={status_available < 2}
                    />
                    <PowerButton
                        isOn={power3}
                        position={3}
                        // onChange={value => {
                        //     setPower3(!power3)
                        // }}
                        boardId={board_id}
                        isUnavailable={status_available < 3}
                    />

                </div>
            </div>
        </SwitchWrapper>
    )
}

export default SwitchItem