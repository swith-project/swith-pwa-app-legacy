import React from 'react'
import styled from 'styled-components'
import { MOBILE_BREAKPOINT } from '../../constants'
import { NavLink, useHistory } from 'react-router-dom'
import * as Icon from 'react-feather'

const Navbar = {

    Wrapper: styled.nav`
    //margin-bottom: 5px;
    
    flex: 1;
    align-self: flex-start;
    padding: 0 3rem;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 10;
    background: rgba(255,255,255,1);
    
     // 40em == 640px
    @media only screen and (max-width: ${MOBILE_BREAKPOINT}) {
      position: fixed;
      width: 100vw;
      bottom: 0;
      justify-content: center;
      padding-bottom: env(safe-area-inset-bottom);
    }
  `,
    Logo: styled.h2`
    border: 1px solid gray;
    padding: 0.05rem 1rem;
    margin-bottom: 0;
    border-radius: 10px;
    @media only screen and (max-width: ${MOBILE_BREAKPOINT}) {
     display: none;
     
    }
  `,
    Items: styled.ul`
    display: flex;
    list-style: none;
    justify-content: center;
    margin-bottom: 0;
    @media only screen and (max-width: ${MOBILE_BREAKPOINT}) {
      width: 100%;
    }
  `,
    Item: styled.li`
    
    a {
         display: flex;
    align-items: center;
     padding: 1rem 1.75rem;  
      text-decoration: none;
      color: black;
      cursor: pointer;
      .anticon{
     display:none;
     
    }
    svg {
    margin-right: 5px;
    }
    //vertical-align: middle;
   
    &.active{
      border-top: 2px solid #1890ff;
    }
    
    @media only screen and (max-width: ${MOBILE_BREAKPOINT}) {
      .anticon{
        display:block;
        font-size: 1.5rem;
      }
      font-size: 0.95rem;
    }
    &:hover{
      color: #1890ff;
    }
    }
   
  `
}

function Menu () {
    const history = useHistory()
    return (
        <Navbar.Wrapper className={'shadow-lg'}>
            <Navbar.Logo>SWiTH</Navbar.Logo>
            <Navbar.Items>

                <Navbar.Item>
                    <NavLink to={'/family'}>
                        <Icon.Users size={16}/> Family
                    </NavLink>
                </Navbar.Item>
                <Navbar.Item>
                    <NavLink to={'/'}
                             exact
                             activeClassName={'active'}>
                        <Icon.Home size={16}/> {` `}Home
                    </NavLink>
                </Navbar.Item>

                <Navbar.Item>
                    <NavLink to={'/settings'}>
                        <Icon.Settings size={16}/> Settings
                    </NavLink>
                </Navbar.Item>
                {/*<Navbar.Item>*/}
                {/*    <a href={''}*/}
                {/*       className={'nav-link'}*/}
                {/*       onClick={e => {*/}
                {/*           e.preventDefault()*/}
                {/*           localStorage.removeItem('accessToken')*/}
                {/*           userStore.logout()*/}
                {/*           history.push('/login')*/}
                {/*       }}>*/}
                {/*        Logout*/}
                {/*    </a>*/}
                {/*</Navbar.Item>*/}

            </Navbar.Items>
        </Navbar.Wrapper>
    )
}

export default Menu