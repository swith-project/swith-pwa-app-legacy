import React from 'react'
import * as Icon from 'react-feather'

function SettingPage () {
    return (
        <div className={'container-fluid'}>
            <div className="card r-10">
                <div className="card-body">
                    <h3 className={'d-flex align-items-center'}>
                        <Icon.Settings/> <span className={'ml-2'}>Settings</span>
                    </h3>

                </div>
            </div>
        </div>
    )
}

export default SettingPage