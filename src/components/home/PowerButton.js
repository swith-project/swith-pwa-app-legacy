import React, { useState } from 'react'
import * as Icon from 'react-feather'
import { requestWithToken } from '../../requests/requests'

function PowerButton({ boardId = '002482124600000000', isOn = true, isUnavailable = false, position = 1, onChange }) {

    const [loading, setLoading] = useState(false)

    const handleClick = async e => {
        e.preventDefault()
        if (isUnavailable) return true
        setLoading(true)
        await requestWithToken.get(`/robot/dev/${boardId}`, {
            params: {
                direction: isOn ? '0' : '1',
                number: position
            }
        }).then(({ data }) => {
           if(onChange) onChange()
        }).catch(error => {
            if(onChange) onChange()
        })

        setLoading(false)
    }
    if (loading) {
        return <a href=""
                  className={'text-primary'}
        >
            {!isOn ? <Icon.ToggleRight size={35}/> : <Icon.ToggleLeft size={35}/>}
        </a>
    }
    return (
        <a href=""
           className={isUnavailable ? 'text-muted' : isOn ? 'text-success' : 'text-danger'}
           onClick={handleClick}>
            {isOn ? <Icon.ToggleRight size={35}/> : <Icon.ToggleLeft size={35}/>}
        </a>
    )
}

export default PowerButton