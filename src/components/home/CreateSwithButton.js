import React from 'react'
import { SwitchWrapper } from '../switch/SwitchItem'
import * as Icon from 'react-feather'
import { Link } from 'react-router-dom'

function CreateSwithButton() {

    return (
        <Link to={'/new/device'}
              className={'col-6 d-flex'}>
            <SwitchWrapper
                className={'w-100 bg-white d-flex flex-column justify-content-center align-items-center'}>
                <Icon.Plus className={'mt-1 text-muted'}
                           size={60}/>
                <p className={'mt-3 text-muted'}>
                    Add new device
                </p>
            </SwitchWrapper>
        </Link>
    )
}

export default CreateSwithButton