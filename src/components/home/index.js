import React from 'react'
import SwitchItem from '../switch/SwitchItem'
import useSWR from 'swr'
import { fetcher } from '../../requests/requests'
import SwithItemSkeleton from '../skeleton/swith-item'
import CreateSwithButton from './CreateSwithButton'

function Home () {
    const { data, error } = useSWR('/robot/me', fetcher)

    return (
        <div className={'container-fluid'}>
            <div className="row">
                {!data && [...Array(5)].map((_, key) => (
                    <SwithItemSkeleton key={key}/>
                ))}
                {!error && data && data.map(item => (
                    <div className="col-6"
                         key={item.id}>
                        <SwitchItem {...item}/>
                    </div>
                ))}
                {!error && data && <CreateSwithButton/>}
                {error && <p>
                    Sorry, something wrong
                </p>}
            </div>
        </div>
    )
}

export default Home