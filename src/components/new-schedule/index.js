import React, {Fragment,useState,useEffect} from 'react'
import BackButton from '../switch/BackButton'
import ScheduleForm from './ScheduleForm'

function NewSchedule(){
    return (
        <div className={'container-fluid'}>
            <div className={'row'}>
                <div className="col-3 btn-group-sm">
                    <BackButton/>
                </div>
                <div className="col-9 d-flex align-items-center">
                    <h4>
                        New Schedule
                    </h4>
                </div>
                <div className="col-12 mb-5">
                    <div className="card r-10 mt-4">
                        <div className="card-body">
                            <ScheduleForm />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default NewSchedule