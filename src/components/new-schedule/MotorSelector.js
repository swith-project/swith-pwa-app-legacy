import React from 'react'
import * as Icon from 'react-feather'

const renderPower = isOn => isOn ? <Icon.Check size={30}/> : <Icon.ToggleLeft size={30}/>

function MotorSelector({ onChange, value }) {

    return (
        <div className="form-group">
            <div className="btn-group w-100">
                <button
                    onClick={e => {
                        e.preventDefault()
                        onChange(1)
                    }}
                    type="button"
                    className={
                        value === 1 ? `btn text-primary` : `btn`
                    }
                >
                    {renderPower(value === 1)} 1
                </button>
                <button
                    onClick={e => {
                        e.preventDefault()
                        onChange(2)
                    }}
                    type="button"
                    className={
                        value === 2 ? `btn text-primary` : `btn`
                    }
                >
                    {renderPower(value === 2)} 2
                </button>
                <button
                    onClick={e => {
                        e.preventDefault()
                        onChange(3)
                    }}
                    type="button"
                    className={
                        value === 3 ? `btn text-primary` : `btn `
                    }
                >
                    {renderPower(value === 3)} 3
                </button>
            </div>
        </div>
    )
}

export default MotorSelector