import React, { useState } from 'react'
import MotorSelector from './MotorSelector'
import LabelInput from './Label'
import DateTimeScroll from '../manage/DateTimeScroll'
import DayRepeatCheckBox from '../manage/DayRepeatCheckBox'


function ScheduleForm() {

    const [motor, setMotor] = useState(null)
    const [label, setLabel] = useState('Alarm')
    return (
        <form action="">
            <LabelInput
                value={label}
                onChange={s => setLabel(s)}
            />
            <DateTimeScroll/>
            <MotorSelector
                onChange={value => {
                    setMotor(value)
                }}
                value={motor}
            />
            {/*<MotorMode />*/}
            <DayRepeatCheckBox/>


        </form>
    )
}

export default ScheduleForm