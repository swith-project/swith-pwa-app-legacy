import React from 'react'

function LabelInput({ value, onChange }) {
    return (
        <div className=" form-group">
            <div className="input-group">
                <div className="input-group-prepend">
                            <span className="input-group-text">
                               <b>Name</b>
                            </span>
                </div>
                <input
                    type="text"
                    maxLength={50}
                    name={'label'}
                    value={value}
                    onChange={event => {
                        onChange(event.target.value)
                    }}
                    className={'form-control only-border-bottom text-right'}
                />
            </div>
        </div>
    )
}

export default LabelInput