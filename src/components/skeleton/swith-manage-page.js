import React from 'react'
import Skeleton from 'react-loading-skeleton'

function SwitchManagePageSkeleton () {
    return (
        <div>
            <div className="card r-10 mt-4">
                <div className="card-body">
                    {[...Array(3)].map(() => (
                        <div className="row switch-line">
                            <div className="col-8">
                                <h5 className={'mb-0'}>
                                    <Skeleton
                                        height={'28px'}
                                    />
                                </h5>
                            </div>
                            <div className="col-4 text-right d-flex justify-content-end align-items-end">
                                <Skeleton width={'54px'}
                                          height={'28px'}/>
                            </div>
                            <div className="col-12">
                                <hr className={'my-0'}/>
                            </div>
                        </div>
                    ))}

                </div>
            </div>
            <div className="card r-10 mt-4">
                <div className="card-body">
                    <h5>
                        <Skeleton/>
                    </h5>
                    <form action=""
                          className="mt-4">

                        <div className=" form-group">
                            <Skeleton height={'30px'}/>
                        </div>
                        <div className=" form-group">
                            <Skeleton height={'30px'}/>
                        </div>
                        <div className=" form-group">
                            <div className="btn-group w-100">
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                            </div>
                        </div>
                        <div className=" form-group">
                            <div className="btn-group w-100">
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                                <div className="col-4">
                                    <Skeleton height={'30px'}/>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-end">
                            <Skeleton height={'30px'}
                                      width={'55px'}/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SwitchManagePageSkeleton