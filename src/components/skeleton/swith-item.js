import React from 'react'
import { SwitchWrapper } from '../switch/SwitchItem'
import Skeleton from 'react-loading-skeleton'

function SwithItemSkeleton() {
    return (
        <div className="col-6">
            <SwitchWrapper className={'bg-white border-0'}
                           style={{
                               boxShadow: 'none'
                           }}>
                <div className={'header text-center'}>
                    <h4>
                        <Skeleton/>
                    </h4>
                    <p>
                        <Skeleton/>
                    </p>
                </div>
                <div>
                    <div className="d-flex justify-content-between">
                        <Skeleton width={'40px'}
                                  height={35}/>
                        <Skeleton height={35}
                                  width={'40px'}/>
                        <Skeleton height={35}
                                  width={'40px'}/>
                    </div>
                </div>
            </SwitchWrapper>
        </div>
    )
}

export default SwithItemSkeleton