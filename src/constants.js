// export const MOBILE_BREAKPOINT = `640px`
export const MOBILE_BREAKPOINT = `768px`

export const REDIRECT_URI = process.env.NODE_ENV === 'production' ? 'https://swith.online/login' : window.location.href
// `http://103.27.202.4:3000/`
export const SOCKET_IO = process.env.NODE_ENV === 'production' ? `https://api.swith.online/` : `https://api.swith.online/`
